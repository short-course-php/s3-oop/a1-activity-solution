<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity 3</title>
</head>
<body>

	<div style="display: flex; justify-content: space-evenly;">
		<div>
			<h3>Person</h3>
			<h4><?= $person->printName(); ?></h4>
		</div>

		<div>
			<h3>Developer</h3>
			<h4><?= $devs->printName(); ?></h4>
		</div>

		<div>
			<h3>Engineer</h3>
			<h4><?= $eng->printName(); ?></h4>
		</div>
		
	</div>

</body>
</html>